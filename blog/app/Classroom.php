<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'picture', 'address', 'id'
    ];

    public function exercise() {
        return $this->hasMany('App\Exercise');
    }
    
    public function getname() {
		return $this->name;
	}
}