<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Childgroup extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'age'
    ];

    public function exercise() {
    	return $this->hasMany('App\Exercise');
    }
    
    public function getname() {
		return $this->name;
	}
}