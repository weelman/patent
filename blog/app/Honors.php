<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Honors extends Model
{
    
    protected $dates = ['created_at'];
    
	protected $fillable = ['id', 'body', 'category'];
    
}
