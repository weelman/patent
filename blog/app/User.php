<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'fio', 'education', 'post', 'picture', 'is_teacher', 'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function exercises() {
        return $this->hasMany('App\Exercise');
    }
    

	public function getPicAttribute() {
		if (!$this->picture)
			return "user_pic_default.png";
		return $this->picture;
	}
	
	
}
