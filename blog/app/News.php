<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class News extends Model
{
	
	protected $fillable = ['id', 'title', 'body'];
	
	public static $rules = [
		'title' => 'required|max:100',
        'body' => 'text'
	];	
}
