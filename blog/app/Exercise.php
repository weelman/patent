<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise extends Model
{
	use SoftDeletes;


	protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'repeattype', 'accesstype', 'time', 'title', 'desc', 'color', 'user_id', 'childgroup_id', 'classroom_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function childgroup() {
        return $this->belongsTo('App\Childgroup');
    }
    public function classroom() {
        return $this->belongsTo('App\Classroom');
    }
    public function getChildgroupnameAttribute() {
		//dd($this);
        if ($this->childgroup_id)
//            return "Не работает";
             return $this->childgroup->name . "   (" . $this->childgroup->age . " лет )";
        else
            return "Имя группы неизвестно";
    }
    public function getClassroomnameAttribute() {
        if ($this->classroom_id)
            return $this->classroom->name;
//            return "Не работает";
        else
            return "Аудитория не определена";
    }
    public function getUsernameAttribute() {
        if ($this->user_id)
            return $this->user->fio;
        else
            return "Преподаватель не определен";
    }
}
