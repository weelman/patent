<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\News;
use Input;
use DB;
use Log;

class NewsitemController extends Controller
{
   
    
    public function index()
	{
		$news_list = News::all();
		return view('main.mainnews')->with('news_list',$news_list);
	}
    
    public function viewNews()
    {
        $news_list = News::all();
        return view('lk.viewnews')->with('news_list', $news_list);
    }
    public function addNews()
    {
        $news_item = News::all();
        return view('lk.addnews')->with('news_item', $news_item);
    }
    public function postNew()
    {
        $news_item = new News([
			'title' => Input::get('title'),
			'body' => Input::get('body')
		]);
		
		$news_item->save();
		
		return \Redirect::route('viewnews');   
    }
    public function postEdit()
    {
		$newsid = Input::get('newsitemid');
		
        $news_item = News::findOrFail($newsid);
		
        $news_item->title = Input::get('title');
		$news_item->body = Input::get('body');
		
		$news_item->save();
		
		return \Redirect::route('viewnews');   
    }
    
    public function postDelete($id)
    {
		$news_item = News::findOrFail($id);
		$news_item->delete();
		return \Redirect::route('viewnews');
	}
}
