<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Exercise;
use App\Classroom;
use Input;
use Log;

class ClassroomController extends Controller
{
    public function addRoom()
    {
        $room_item = Classroom::all();
        return view('lk.addroom')->with('room_list', $room_item);
    }
    public function getRoom()
    {	
        $room_list = Classroom::all();
        return view('main.searchbyrooms')->with('room_list', $room_list);
    }
    public function getExerByRoom()
	{
		$room_list = Classroom::all()->first();
		return redirect('byrooms/'.$room_list->id);
	}
     public function getExerByRoomId($roomid)
	{
		$classroom = Classroom::find($roomid);
		$exer_list = Exercise::where('classroom_id',$classroom->id)->get();
		$room_list = Classroom::all();
		return view('main.searchbyrooms')->with('exer_list', $exer_list)
										 ->with('room_list', $room_list)
										 ->with('classroom', $classroom);
    }
    public function viewRoom()
    {	
        $room_item = Classroom::all();
        return view('lk.viewroom')->with('room_list', $room_item);
    }
    public function postEdit()
    {
		$roomid = Input::get('roomitemid');
		
        $room_item = Classroom::findOrFail($roomid);
		
		$room_item->name = Input::get('name');
		$room_item->address = Input::get('address');
        $room_item->picture = Input::get('picture');
		
		$room_item->save();
		
		return \Redirect::route('viewroom');   
    }
    
    public function postNew()
    {
        $room_item = new Classroom([
			'name' => Input::get('name'),
			'address' => Input::get('address'),
            'picture' => Input::get('picture')
		]);
		
		$room_item->save();
		
		return \Redirect::route('viewroom');   
    }
    public function postDelete($id)
    {
		$room_item = Classroom::findOrFail($id);
		$room_item->delete();
		return \Redirect::route('viewroom');
	}
    
}
