<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Honors;
use Input;
use DB;
use Log;

class HonorsitemController extends Controller
{
    public function createHonorsitem()
	{
		$honorss = new Honorsitem([
            'id' => Input::get('id'),
			'body' => Input::get('body'),
            'category' => Input::get('category')
		]);
		
		$honorss->save();
        
		return \Redirect::route('mainhonors');
	}
    
    public function index()
	{
        $itemhonors = DB::select('SELECT * FROM honors');
        Log::info($itemhonors);
		$honors_item = Honors::all();
        
		return view('main.mainhonors')->with('honors_list',$honors_item)->with('itemhonors',$itemhonors);
	}
		
}
