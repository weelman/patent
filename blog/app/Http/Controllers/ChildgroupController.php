<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Childgroup;
use App\Exercise;
use Input;

class ChildgroupController extends Controller
{
    public function addGroup()
    {
        $group_item = Childgroup::all();
        return view('lk.addgroup')->with('group_list', $group_item);
    }
    public function getExerByGroup()
	{
		$group_list = Childgroup::all()->first();
		return redirect('bygroups/'.$group_list->id);
	}
     public function getExerByGroupId($groupid)
	{
		$childgroup = Childgroup::find($groupid);
		$exer_list = Exercise::where('childgroup_id',$childgroup->id)->get();
		$group_list = Childgroup::all();
		return view('main.searchbygroups')->with('exer_list', $exer_list)
										 ->with('group_list', $group_list)
										 ->with('childgroup', $childgroup);
    }
    public function viewGroup()
    {	
        $group_item = Childgroup::all();
        return view('lk.viewgroup')->with('group_list', $group_item);
    }
    public function postNew()
    {
		
        $group_item = new Childgroup([
			'name' => Input::get('name'),
			'age' => Input::get('age')
		]);
		
		
		$group_item->save();
		
		return \Redirect::route('viewgroup');   
    }

	
    public function postEdit()
    {
		$grpid = Input::get('groupitemid');
		
        $group_item = Childgroup::findOrFail($grpid);
		
		$group_item->name = Input::get('name');
		$group_item->age = Input::get('age');
		
		$group_item->save();
		
		return \Redirect::route('viewgroup');   
    }


	public function postDelete($id)
    {
		$group_item = Childgroup::findOrFail($id);
		$group_item->delete();
		return \Redirect::route('viewgroup');
	}

}
