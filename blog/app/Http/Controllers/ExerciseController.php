<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Exercise;
use App\User;
use App\Childgroup;
use Input;
use App\Classroom;

use Log;

class ExerciseController extends Controller
{
    public function viewExer()
	{	
        $user_list = User::all();
        $exer_list = Exercise::all();
        return view('lk.viewexer')->with('exer_list', $exer_list)->with('user_list', $user_list);
    }


	public function getExerByTeach()
	{
		$firstteacher = User::where('is_teacher',true)->get()->first();
		return redirect('byteach/'.$firstteacher->id);
	}

	
    public function getExerByTeachId($teachid)
	{
		$teacher = User::find($teachid);
		$exer_list = Exercise::where('user_id',$teacher->id)->get();
		$user_list = User::where('is_teacher',true)->get();
		return view('main.searchbyteach')->with('exer_list', $exer_list)
										 ->with('user_list', $user_list)
										 ->with('teacher', $teacher);
    }
    public function addExer()
    {
        $exer_item = Exercise::all();
        $room_list = Classroom::all();
        $group_list = Childgroup::all();
        $teach_list = User::all();
        return view('lk.addexer')->with('exer_item', $exer_item)
                                 ->with('room_list', $room_list)
                                 ->with('group_list', $group_list)
                                 ->with('teach_list', $teach_list);
    }
    
    public function postNew()
    {
        $exer_item = new Exercise([
			'title' => Input::get('title'),
			'desc' => Input::get('desc'),
            'color' => Input::get('color'),
            'time' => Input::get('time'),
            'repeattype' => Input::get('repeattype'),
            'accesstype' => Input::get('accesstype'),
            'classroom_id' => Input::get('classroom_id'),
            'childgroup_id' => Input::get('childgroup_id'),
            'user_id' => Input::get('user_id'),
		]);
		
		$exer_item->save();
		
		return \Redirect::route('viewexer');   
    }
    public function postEdit()
    {
		$exerid = Input::get('exeritemid');
		
        $exer_item = Exercise::findOrFail($exerid);
		
        $exer_item->title = Input::get('title');
		$exer_item->desc = Input::get('desc');
		$exer_item->color = Input::get('color');
        $exer_item->time = Input::get('time');
        $exer_item->repeattype = Input::get('repeattype');
        $exer_item->accesstype = Input::get('accesstype');
        $exer_item->classroom_id = Input::get('classroom_id');
        $exer_item->childgroup_id = Input::get('childgroup_id');
        $exer_item->user_id = Input::get('user_id');
		
		$exer_item->save();
		
		return \Redirect::route('viewexer');   
    }
    
    public function postDelete($id)
    {
		$exer_item = Exercise::findOrFail($id);
		$exer_item->delete();
		return \Redirect::route('viewexer');
	}
}