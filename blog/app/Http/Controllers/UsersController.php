<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\User;
use Input;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_item = User::all();
		return view('main.searchbyteach')->with('user_list',$user_item);
    }

    public function viewTeach()
    {   
        $teach_list = User::all();
        return view('lk.viewteach')->with('teach_list', $teach_list);
    }

    public function addTeach()
    {
        $teach_item = User::all();
        return view('lk.addteach')->with('teach_list', $teach_item);
    }
    
    public function postEdit()
    {
		$teachid = Input::get('teachitemid');
		
        $teach_item = User::findOrFail($teachid);
		
		$teach_item->name = Input::get('name');
        $teach_item->password = bcrypt(Input::get('password'));
		$teach_item->fio = Input::get('fio');
        $teach_item->email = Input::get('email');
        $teach_item->education = Input::get('education');
        $teach_item->post = Input::get('post');
		
		$teach_item->save();
		
		return \Redirect::route('viewteach');   
    }
    public function postNew()
    {
        $teach_item = new User([
			'name' => Input::get('name'),
			'password' => bcrypt(Input::get('password')),
            'fio' => Input::get('fio'),
            'email' => Input::get('email'),
            'education' => Input::get('education'),
            'post' => Input::get('post'),
            
		]);
		
		$teach_item->save();
		
		return \Redirect::route('viewteach');   
    }
    public function postDelete($id)
    {
		$teach_item = User::findOrFail($id);
		$teach_item->delete();
		return \Redirect::route('viewteach');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    
	public function getTeach()
	{
		
		$id = Input::get('teach_id');
		$teacher = User::find($id);
		
		if (!$teacher) {
			$res = [ "result" => false];
			return JsonResponse::create( $res, 200 );
		}
		$teacher2 = $teacher->toArray();
		$teacher2["pic"] = $teacher->pic;
		$res = [ "result" => true, "teacher" => $teacher2];
		return JsonResponse::create( $res, 200 );
	}
    
}
