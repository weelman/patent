<?php



Route::get('/', function () {
    return Redirect::to('index');
});

Route::any('home', function(){
    return view('/home');       
});


Route::group(['prefix'=>'lk', 'middleware' => 'auth'], function () {
    
	Route::get('exercise', ['as' => 'viewexer', 'uses' => 'ExerciseController@viewExer']);
	Route::get('classrooms', ['as' => 'viewroom', 'uses' => 'ClassroomController@viewRoom']);
	Route::get('childgroup', ['as' => 'viewgroup', 'uses' => 'ChildgroupController@viewGroup']);
	Route::get('teachers', ['as' => 'viewteach', 'uses' => 'UsersController@viewTeach']);
	Route::get('news', ['as' => 'viewnews', 'uses' => 'NewsitemController@viewNews']);

	Route::get('addroom', ['as' => 'addroom', 'uses' => 'ClassroomController@addRoom']);
	Route::get('addgroup', ['as' => 'addgroup', 'uses' => 'ChildgroupController@addGroup']);
	Route::get('addexer', ['as' => 'addexer', 'uses' => 'ExerciseController@addExer']);
	Route::get('addteach', ['as' => 'addteach', 'uses' => 'UsersController@addTeach']);
	Route::get('addnews', ['as' => 'addnews', 'uses' => 'NewsitemController@addNews']);

	Route::get('rooms/edit/{id}',['as' => 'editroom', function($id) {
		$room_item = App\Classroom::find($id);
		return view('lk.editroom')->with('room_item', $room_item);
	}]);
	Route::post('rooms/edit', ['as'=> 'roomedit', 'uses'=>'ClassroomController@postEdit']);
	Route::get('rooms/delete/{id}', 'ClassroomController@postDelete');
	Route::post('room/add', ['as'=> 'roomadd', 'uses'=>'ClassroomController@postNew']);

	Route::get('group/edit/{id}',['as' => 'editgroup', function($id) {
		$group_item = App\Childgroup::find($id);
		return view('lk.editgroup')->with('group_item', $group_item);
	}]);
	Route::post('group/edit', ['as'=> 'groupedit', 'uses'=>'ChildgroupController@postEdit']);
	Route::get('group/delete/{id}', 'ChildgroupController@postDelete');
	Route::post('group/add', ['as'=> 'groupadd', 'uses'=>'ChildgroupController@postNew']);

	Route::get('exer/edit/{id}',['as' => 'editexer', function($id) {
		$exer_item = App\Exercise::find($id);
		$room_list = App\Classroom::all();
		$group_list = App\Childgroup::all();
		$teach_list = App\User::all();
		return view('lk.editexer')->with('exer_item', $exer_item)->with('room_list', $room_list)->with('group_list', $group_list)->with('teach_list', $teach_list);
	}]);
	Route::post('exer/edit', ['as'=> 'exeredit', 'uses'=>'ExerciseController@postEdit']);
	Route::post('exer/add', ['as'=> 'exeradd', 'uses'=>'ExerciseController@postNew']);
	Route::get('exer/delete/{id}', 'ExerciseController@postDelete');

	Route::get('teach/edit/{id}',['as' => 'editteach', function($id) {
		$teach_item = App\User::find($id);
		return view('lk.editteach')->with('teach_item', $teach_item);
	}]);
	Route::post('teach/edit', ['as'=> 'teachedit', 'uses'=>'UsersController@postEdit']);
	Route::post('teach/add', ['as'=> 'teachadd', 'uses'=>'UsersController@postNew']);
	Route::get('teach/delete/{id}', 'UsersController@postDelete');

	Route::get('news/edit/{id}',['as' => 'editnews', function($id) {
		$news_item = App\News::find($id);
		return view('lk.editnews')->with('news_item', $news_item);
	}]);
	Route::post('news/edit', ['as'=> 'newsedit', 'uses'=>'NewsitemController@postEdit']);
	Route::post('news/add', ['as'=> 'newsadd', 'uses'=>'NewsitemController@postNew']);
	Route::get('news/delete/{id}', 'NewsitemController@postDelete');
});

Route::get('teach/getjson', 'UsersController@getTeach');

Route::get('/', function () {
	//$teach_list = App\User::all();
	$teach_list = App\User::where('is_teacher',true)->get();
	return view('main.mainindex')->with('teach_list', $teach_list);
});
    
Route::get('news', ['as' => 'mainnews', 'uses' => 'NewsitemController@index'
]);
    
Route::get('gallery', ['as' => 'maingallery', function () {
	return view('main.maingallery');
}]);
    
Route::get('honors', ['as' => 'mainhonors', 'uses' => 'HonorsitemController@index'
]);
    
Route::get('news/{id}', function ($id) {
	$srv = App\News::find($id);
	return view('main.mainnewsview')->with('news', $srv);
});
    
Route::get('guestbook', ['as' => 'mainguestbook', function () {
	return view('main.mainguestbook');
}]);
    
Route::get('repertoire', ['as' => 'mainrepertoire', function () {
	return view('main.mainrepertoire');
}]);
    
Route::get('byrooms', ['as' => 'byrooms', 'uses' => 'ClassroomController@getExerByRoom']);

Route::get('byrooms/{id}', 'ClassroomController@getExerByRoomId');

Route::get('byteach', ['as' => 'byteach', 'uses' => 'ExerciseController@getExerByTeach']);

Route::get('byteach/{id}', 'ExerciseController@getExerByTeachId');
    
Route::get('bygroups', ['as' => 'bygroups', 'uses' => 'ChildgroupController@getExerByGroup']);

Route::get('404', ['as' => '404', function () {
    return view('404');
}]);

Route::get('bygroups/{id}', 'ChildgroupController@getExerByGroupId');
    
Route::get('setagroop', ['as' => 'mainsetagroop', function () {
	return view('main.mainsetagroop');
}]);
    
Route::get('teachers', ['as' => 'mainteachers', function () {
	return view('main.mainteachers');
}]);
    
Route::get('contacts', ['as' => 'maincontacts', function () {
	return view('main.maincontacts');
}]);
    
Route::get('map', ['as' => 'mainmap', function () {
	return view('main.mainmap');
}]);

Route::auth();