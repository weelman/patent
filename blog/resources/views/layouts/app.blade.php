<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Личный кабинет</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> {!! Html::style('css/main.css') !!} 
    {!! Html::style('css/lk.css') !!}
    {!! Html::style('css/bootstrap-colorpicker.min.css') !!}
    
    {!! Html::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') !!}
    <style>
        body {
            font-family: 'Lato';
        }
        
        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

<body id="app-layout">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Навигация</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('login') }}" style="font-size: 16px">На главную</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}" style="font-size: 16px">На основной сайт</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Войти</a></li>
<!--                    <li><a href="{{ url('/register') }}">Регистрация</a></li>-->
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Выйти</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
                
            </div>
        </div>
    </nav>

    <div class="container">
        @if (!Auth::guest())
        <ul class="nav nav-tabs nav-justified round-tab">
            <li role="presentation"><a href="{{ URL::route('viewexer') }}">Занятия</a></li>
            <li role="presentation"><a href="{{ URL::route('viewroom') }}">Аудитории</a></li>
            <li role="presentation"><a href="{{ URL::route('viewgroup') }}">Группы</a></li>
            <li role="presentation"><a href="{{ URL::route('viewteach') }}">Преподаватели</a></li>
            <li role="presentation"><a href="{{ URL::route('viewnews') }}">Новости</a></li>
        </ul>
        @endif 
        @yield('content')
    </div>
    {!! Html::script('js/bootstrap-colorpicker.js') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{--
    <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>

</html>