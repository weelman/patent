@extends('layouts.app') @section('content') {!! Form::open(array('url' => URL::route('exeredit'), 'class'=>'form')) !!}
{{ Form::hidden('exeritemid', $exer_item->id) }}
<div class="panel panel-default">
    <div class="panel-heading">Изменить Занятие</div>
    <div class="panel-body">
        <div class="form-group col-sm-12">
            {{ Form::label('title', 'Название', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::text('title', $exer_item->title, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('desc', 'Описание', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::text('desc', $exer_item->desc, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('classroom_id', 'Аудитория', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                <select class="form-control" name="classroom_id" id="classroom_id">
                    @foreach ($room_list as $room_list)
                    <option value="{{ $room_list->id }}" @if( ($exer_item->classroom_id)===($room_list->id) ) selected @endif >{{ $room_list->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('childgroup_id', 'Группа', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                <select class="form-control" name="childgroup_id" id="childgroup_id">
                    @foreach ($group_list as $group_list)
                    <option value="{{ $group_list->id }}" @if( ($exer_item->childgroup_id)===($group_list->id) ) selected @endif>{{ $group_list->name . ' (' . $group_list->age . ' лет)' }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('user_id', 'Преподаватель', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                <select class="form-control" name="user_id" id="user_id">
                    @foreach ($teach_list as $teach_item)
                    <option value="{{ $teach_item->id }}" @if( ($exer_item->user_id)===($teach_item->id) ) selected @endif>{{ $teach_item->fio }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('repeattype', 'Частота занятий', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                 {{ Form::text('repeattype', $exer_item->repeattype, array('class'=>'form-control')) }} 
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('accesstype', 'Тип занятий', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                 {{ Form::text('accesstype', $exer_item->accesstype, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('color', 'Цвет фона', array('class'=>'col-sm-2 control-label'))}}
            <div id="cp2" class="col-sm-5 input-group colorpicker-component" style="padding-left: 14px;">
                {{ Form::text('color', $exer_item->color, array('class'=>'form-control')) }}
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('time', 'Время', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-2">
                {{ Form::text('time', $exer_item->time, array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-item col-sm-12">
            {{ Form::submit('Сохранить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#cp2').colorpicker();
    });
</script>
{!! Form::close() !!} @endsection