@extends('layouts.app') @section('content')


        {!! Form::open(array('url' => URL::route('roomedit'), 'class'=>'form')) !!}
        {{ Form::hidden('roomitemid', $room_item->id) }}
        <div class="panel panel-default">
            <div class="panel-heading">Изменить аудиторию</div>
            <div class="panel-body">
                <div class="form-group col-sm-12">
                    {{ Form::label('name', 'Аудитория', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('name', $room_item->name, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('address', 'Адрес', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('address', $room_item->address, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('picture', 'Ссылка на изображение', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('picture', $room_item->picture, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::submit('Сохранить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
@endsection