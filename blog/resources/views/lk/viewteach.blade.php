@extends('layouts.app') @section('content')
@if(is_null($teach_list))
    <h1>Пусто</h1>
    <a href='{{ URL::route('addteach') }}' class='btn btn-success'>Добавить преподавателя</a>
@else
<table class="table table-stripped">
    <thead>
        <tr>
            <th>Логин</th>
            <th>ФИО</th>
            <th>Почта</th>
            <th>Должность</th>
            <th>Образование</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($teach_list as $teach_list)
        <tr>
            <td>{{ $teach_list->name }}</td>
            <td>{{ $teach_list->fio }}</td>
            <td>{{ $teach_list->email }}</td>
            <td>{{ $teach_list->post }}</td>
            <td>{{ $teach_list->education }}</td>
            <td>
                <a href='teach/edit/{{ $teach_list->id }}' class='btn btn-default'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $teach_list->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            </td>
        </tr>
        <div class="modal fade bs-example-modal-sm-{{ $teach_list->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                        <a href='teach/delete/{{ $teach_list->id }}'><button type="button" class="btn btn-success">Да</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </tbody>
</table>
<a href='{{ URL::route('addteach') }}' class='btn btn-success'>Добавить преподавателя</a>


@endif
@endsection