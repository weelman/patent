@extends('layouts.app') @section('content')
@if(is_null($group_list))
    <h1>Пусто</h1>
    <a href='{{ URL::route('addgroup') }}' class='btn btn-success'>Добавить группу</a>
@else
<table class="table table-stripped">
    <thead>
        <tr>
            <th>Название</th>
            <th>Возраст</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($group_list as $group_item)
        <tr>
            <td><strong>{{ $group_item->name }}</strong></td>
            <td>{{ $group_item->age }}</td>
            <td>
                <a href='group/edit/{{ $group_item->id }}' class='btn btn-default'>
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                    <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $group_item->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            </td>
        </tr>
        <div class="modal fade bs-example-modal-sm-{{ $group_item->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                  <a href='group/delete/{{ $group_item->id }}'><button type="button" class="btn btn-success">Да</button></a>
              </div>
            </div>
          </div>
        </div>
    @endforeach
    </tbody>
</table>

<a href='{{ URL::route('addgroup') }}' class='btn btn-success'>Добавить группу</a>



@endif
@endsection
