@extends('layouts.app') @section('content')
@if(is_null($news_list))
    <h1>Пусто</h1>
    <a href='{{ URL::route('addnews') }}' class='btn btn-success'>Добавить аудиторию</a>
@else
<table class="table table-stripped">
    <thead>
        <tr>
            <th>Название</th>
            <th>Описание</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($news_list as $news_list)
        <tr>
            <td><strong>{{ $news_list->title }}</strong></td>
            <td>{{ $news_list->body }}</td>
            <td>
                <a href='news/edit/{{ $news_list->id }}' class='btn btn-default'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $news_list->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            </td>
        </tr>
        <div class="modal fade bs-example-modal-sm-{{ $news_list->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                        <a href='news/delete/{{ $news_list->id }}'>
                            <button type="button" class="btn btn-primary">Да</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </tbody>
</table>
<a href='{{ URL::route('addnews') }}' class='btn btn-success'>Добавить аудиторию</a>



@endif
@endsection