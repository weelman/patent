@extends('layouts.app') @section('content') {!! Form::open(array('url' => URL::route('exeradd'), 'class'=>'form')) !!}
<div class="panel panel-default">
    <div class="panel-heading">Добавить занятие</div>
    <div class="panel-body">
        <div class="form-group col-sm-12">
            {{ Form::label('title', 'Название', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::text('title', null, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('desc', 'Описание', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::text('desc', null, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('classroom_id', 'Аудитория', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                <select class="form-control" name="classroom_id" id="classroom_id">
                    @foreach ($room_list as $room_list)
                    <option value="{{ $room_list->id }}" >{{ $room_list->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('childgroup_id', 'Группа', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                <select class="form-control" name="childgroup_id" id="childgroup_id">
                    @foreach ($group_list as $group_list)
                    <option value="{{ $group_list->id }}">{{ $group_list->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('user_id', 'Преподаватель', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                <select class="form-control" name="user_id" id="user_id">
                    @foreach ($teach_list as $teach_list)
                    <option value="{{ $teach_list->id }}">{{ $teach_list->fio }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!--        ##-->
        <div class="form-group col-sm-12">
            {{ Form::label('repeatrype', 'Частота занятий', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::select('repeatrype', ['0'=>'разовое', '1'=>'еженедельное', '2'=>'ежедневное (пн-пт)'], 0, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('accesstype', 'Тип занятий', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-10">
                {{ Form::select('accesstype', ['0'=>'для всех', '1'=>'для группы', '2'=>'индивидуальное'], 0, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('color', 'Цвет фона', array('class'=>'col-sm-2 control-label'))}}
            <div id="cp2" class="col-sm-5 input-group colorpicker-component" style="padding-left: 14px;">
                {{ Form::text('color', '#00AABB', array('class'=>'form-control')) }}
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::label('time', 'Время', array('class'=>'col-sm-2 control-label'))}}
            <div class="col-sm-2">
                {{ Form::text('time', null, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {{ Form::submit('Добавить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#cp2').colorpicker();
    });
</script>
{!! Form::close() !!} @endsection