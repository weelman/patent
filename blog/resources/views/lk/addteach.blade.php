@extends('layouts.app') @section('content')


        {!! Form::open(array('url' => URL::route('teachadd'), 'class'=>'form')) !!}
        <div class="panel panel-default">
            <div class="panel-heading">Добавить преподавателя</div>
            <div class="panel-body">
                <div class="form-group col-sm-12">
                    {{ Form::label('name', 'Логин', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('name', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('password', 'Пароль', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::password('password',  array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('fio', 'ФИО', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('fio', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('email', 'Почта', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('email', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('post', 'Должность', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('post', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('picture', 'Ссылка на фото', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('picture', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('education', 'Образование', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::textarea ('education', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::submit('Сохранить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!}

@endsection