@extends('layouts.app') @section('content')


        {!! Form::open(array('url' => URL::route('teachedit'), 'class'=>'form')) !!}
        {{ Form::hidden('teachitemid', $teach_item->id) }}
        <div class="panel panel-default">
            <div class="panel-heading">Изменить аудиторию</div>
            <div class="panel-body">
                <div class="form-group col-sm-12">
                    {{ Form::label('name', 'Логин', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('name', $teach_item->name, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('fio', 'ФИО', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('fio', $teach_item->fio, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('password', 'Пароль', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::password('password',  array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('email', 'Почта', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('email', $teach_item->email, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('post', 'Должность', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('post', $teach_item->post, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('picture', 'Ссылка на фото', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('picture', $teach_item->picture, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('education', 'Образование', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::textarea ('education', $teach_item->education, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::submit('Сохранить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
@endsection