@extends('layouts.app') @section('content')
@if(is_null($exer_list))
    <h1>Пусто</h1>
    <a href='{{ URL::route('addexer') }}' class='btn btn-success'>Добавить занятие</a>
@else
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Преподаватель</th>
                    <th>Описание</th>
                    <th>Аудитория</th>
                    <th>Частота занятий</th>
                    <th>Тип занятий</th>
                    <th>Время</th>
                    <th>Группа</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($exer_list as $exer_list)
                <tr style="background-color:{{ $exer_list->color }}">
                    <td>{{ $exer_list->title }}</td>
                    <td>{{ $exer_list->username }}</td>
                    <td>{{ $exer_list->desc }}</td>
                    <td>{{ $exer_list->classroomname }}</td>
                    <td>{{ $exer_list->repeattype }}</td>
                    <td>{{ $exer_list->accesstype }}</td>
                    <td>{{ $exer_list->time }}</td>
                    <td>{{ $exer_list->childgroupname }}</td>
                    <td>
                        <a href='exer/edit/{{ $exer_list->id }}' class='btn btn-default'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $exer_list->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    </td>
                </tr>
                <div class="modal fade bs-example-modal-sm-{{ $exer_list->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                          <a href='exer/delete/{{ $exer_list->id }}' class='btn btn-default' data-toggle="modal" data-target="#myModal"><button type="button" class="btn btn-primary">Да</button></a>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
            </tbody>
        </table>
<a href='{{ URL::route('addexer') }}' class='btn btn-success'>Добавить занятие</a>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
          <a href='exer/delete/{{ $exer_list->id }}' class='btn btn-default' data-toggle="modal" data-target="#myModal"><button type="button" class="btn btn-primary">Да</button></a>
      </div>
    </div>
  </div>
</div>

@endif

@endsection