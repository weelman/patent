@extends('main.mainlayout')

@section('sidecontent')

    <div class="row">
        <div class="col-sm-8">
            <h1 class="text-left hedr1">«АРТиШОК»</h1>
            <h3 class="text-left hedr2">Детский хореографический ансамбль</h3>
        </div>
        <div class="col-sm-4">
            <a href="/setagroop" class="btn btn-success btn-lg  dancebut">
                Начни танцевать <br />прямо <span>сейчас!</span>
            </a>
            
        </div>
    </div>


    
    <!-- <div class="container">

         <div class="col-sm-9 col-sm-offset-2">
         <h1 class="text-left">«АРТиШОК»</h1>
         </div>
         
         <div class="col-sm-9 col-sm-offset-2">
         <h3 class="text-left">Детский хореографический ансамбль</h3b>
         </div>   
         </div>
       -->
    
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/photo[1].JPG">
                <div class="carousel-caption">
                </div>
            </div>

            <div class="item">
                <img src="img/photo[2].JPG">
                <div class="carousel-caption">
                </div>
            </div>

            <div class="item">
                <img src="img/photo[3].JPG">
                <div class="carousel-caption">
                </div>
            </div>
        </div>
    </div><!-- /.carousel -->

    
    <div class="row mainpanes">
        <div class="col-md-10 col-md-offset-1">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">О нас</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Достижения</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Атмосфера</a></li>
            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Набор</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <p> Детский хореографический ансамбль «АРТиШОК» 16 марта 2002 года дебютировал на городском конкурсе «Весенняя капель», с тех пор считает данную дату днем своего рождения. </p>
                <p> Основатель коллектива и его руководитель - ЧАРУШИНА Елена Николаевна.</p> <p> В марте 2012 года нам исполнилось 10 лет! И подводя некоторые итоги, мы понимаем, что достигли определенного уровня в обучении детей искусству танца, соответствующего уровню школы.  Как и в любой школе у нас есть программа обучения, в которой предусмотрены уроки по: классическому, эстрадному и современному танцу, гимнастике, актёрскому мастерству, постановочные и репетиционные уроки по сохранению и развитию репертуара коллектива.</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                
                <p>2015 Обладатель Гран-При, Лауреат I, II степени, Дипломант I степени и обладатель специального приза жюри «За костюмы» фестиваля-конкурса в рамках Международного проекта «Сибирь зажигает звезды», г. Красноярск;</p><p>2015 Бронзовый Лауреат Международного конкурса-фестиваля детского и молодежного творчества «Весенние выкрутасы 2015», г. Казань, Республика Татарстан;</p><p>2014 Лауреат II и III степени VII Международного фестиваля-конкурса детского и юношеского творчества «Юные дарования России»;</p><p>2014 Лауреат III степени VII Международного фестиваля-конкурса «ART-прорыв»;</p><p>2015 Лауреат I, III степени XI Всероссийского конкурса детского и юношеского творчества «Москва-Байкальск транзит» г. Байкальск;</p>                
                <p>Лауреат I степени II областного конкурса «Ступеньки к успеху»;</p>
                <p>Победитель конкурса концертных программ «Великая страна – Великая победа»;</p>
                <p></p>
                <p></p>
            </div>
            <div role="tabpanel" class="tab-pane" id="messages">
                <p> Занятия проводят талантливые, опытные и отзывчивые педагоги, основная цель которых,  развить творческий потенциал ребенка, сформировать его активность, самостоятельность, стремление к саморазвитию и творчеству.</p>
                
                <p> Добрая атмосфера внутри коллектива позволяет детям найти много друзей. АРТиШАТ объединяют общие интересы, цели, достижения. Дисциплина, к которой приучены наши дети с первых шагов в танцевальном классе, остается на всю жизнь.</p>
                
                <p> Но наша деятельность не ограничивается  стенами танцевального класса. Совместные празднования Нового года, походы на природу, в кино, музеи, подготовки к концертам, поездки, выступления объединяют  нас. Мы становимся близкими друг другу людьми, друзьями.  Все мы – дети, родители, педагоги становимся одной большой семьей, имя которой АРТиШОК!
                </p>
            </div>
            <div role="tabpanel" class="tab-pane" id="settings">
                <p>  Мы принимаем всех детей. У нас "школа танца", а не "фабрика звезд".</p><p> 

Главная наша задача - развитие ребенка, а не его профессиональная карьера. Желание - это, пожалуй, главный для нас критерий. Но и он не всегда является объективным. Как показывает опыт, у многих наших учеников "аппетит просыпается во время еды".</p><p> 

Ребятишки приходят к нам с 3-х лет (девочки, мальчики). Принимаются в группы подготовительного обучения (по возрасту) на общих условиях.</p><p> 

В 2015 году группы М1 (дети 3 – 4 лет) набирает ГАПШИТЕ Елена Павловна, группы М4 (дети 3-4 лет), M5 (дети 5 – 6 лет) набирает СЕРДЮК Юлия Евгеньевна Красным цветом в расписании выделены группы нового набора</p><p> 

Первые две недели обучения «пробные», форма для занятий свободная. Записаться на «пробные» занятия можно по телефону: 89025689777. Обучение начнется со 2 сентября. ДО ВСТРЕЧИ!!! </p>
            </div>
        </div>  <!-- .tab-content -->
        </div>
    </div> <!-- .mainpanes -->

    <br />

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-sm-5">
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?122"></script>

                    <!-- VK Widget -->
                    <div id="vk_groups"></div>
                    <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 0, width: "340", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 94401127);
                    </script>
                    

                </div>

                <div class="col-sm-7">
                    <iframe width="560" height="400" src="https://www.youtube.com/embed/Z-JKV6r17Nc" frameborder="0" allowfullscreen></iframe>
                </div>
                
            </div>
        </div>
    </div>
    
    <br />
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
        <div class="panel-body">
            <h3 class="lobster">Танцевальный лагерь на Байкале!</h3>
            <img style="width: 500px;  float:left; margin: 10px;" src="/img/bay.jpg">
            <p class="text-justify" style="font-size: 16px; margin-top: 10px">УРА!!! ЛЕТО!!! Мы решили, что это лето станет для нас особенным. Впервые организовали танцевальный лагерь на Байкале! Турбаза «Тогот»: вкусная еда, уютные номера, гостеприимные хозяева и очень живописный уголок байкальской природы... Два класса в день по современному танцу от потрясающего педагога Владимира Алешина г. Москва, интереснейший класс по hand-made от Ольги Хайрутдиновой были нашими ежедневными составляющими. Но у нас еще было время сходить в поход, попариться в бане, покататься на банане и отметить пять дней рождений! Мы купались и загорали каждый день, устраивали дискотеки, костры с песнями и играмиВот это получился отдых!!!</p>
            <p class="text-right">
                <a class="btn btn-primary" href="asd">Перейти к новости</a>
            </p>
            

        </div>
    </div>
        </div>
    </div>




    <div class="container marketing">
        <center><h1 style="font-family: 'lobster'">Наши преподаватели</h1></center>
        <div class="col-sm-8 col-sm-offset-2" style="padding: 20px">
           @foreach ($teach_list as $teach_item)
               <div class="col-sm-4">
                   <img src="/img/teach/{{ $teach_item->pic }}" alt="" class="img-circle" width="140" height="140">
                   <h2 class="main_teach_title"><a href="byteach/{{ $teach_item->id }}">{{ $teach_item->fio }}</a></h2>
               </div>
           @endforeach
        </div>   
    </div>
    <!-- Left and right controls -->



    <br />
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">

                    <p>Контакты: 664074,  Россия, г. Иркутск,  ул. Ломоносова,72;  Тел: 969-858;  E-mail: Boss@artishok-irk.ru;
                    </p>
                    <p>
                        <b>Разработчик: веб-студия Даниила Моисеева</b>
                    </p>
                    

                    

                </div>
            </div>
        </div>
    </div>



@endsection
