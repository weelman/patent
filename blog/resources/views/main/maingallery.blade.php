@extends('main.mainlayout')


@section('sidecontent')

{!! Html::style('assets/swipebox/swipebox.css') !!}
{!! Html::style('assets/css/style.css') !!}

<h1 class="page-header">Галерея</h1>

<div id="loading"></div>

<div id="gallery" style="margin-left: 45px !important; position: static !important; "></div>

{!! Html::script('assets/swipebox/jquery.swipebox.min.js') !!}
{!! Html::script('assets/js/jquery.loadImage.js') !!}
{!! Html::script('assets/js/script.js') !!}

@endsection
