<!DOCTYPE html>
<html lang="ru">
	<head>
		@include("blocks.htmlmeta")
		{!! Html::style('css/bootstrap.min.css') !!}
		{!! Html::style('css/common.css') !!}
		{!! Html::style('css/main.css') !!}
        
      	{!! Html::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') !!}
        {!! Html::script('js/bootstrap.min.js') !!}

        <link href='https://fonts.googleapis.com/css?family=Russo+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

	</head>
    
<body>
    <header>
        <div class="page">
            <nav class="navbar navbar-fixed-top navbar-inverse col-xs-12">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Меню навигации</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a type="button" class="btn btn-lg btn-color" href="/">Главная</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="{{ URL::route('404') }}">Сведения об ОО</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::route('404') }}">Основные сведения</a></li>
                                    <li><a href="{{ URL::route('404') }}">Педагогический состав</a></li>
                                    <li><a href="{{ URL::route('404') }}">Структура и органы управления ОО</a></li>
                                    <li><a href="{{ URL::route('404') }}">Документы</a></li>
                                    <li><a href="{{ URL::route('404') }}">Образовательные стандарты</a></li>
                                    <li><a href="{{ URL::route('404') }}">Материально-техническое обеспечение</a></li>
                                    <li><a href="{{ URL::route('404') }}">Финансово-хозяйственная деятельность</a></li>
                                    <li><a href="{{ URL::route('404') }}">Вакансии</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ URL::route('mainnews') }}">Новости</a>
                            </li>
                            <li class="dropdown">
                                <a href="{{ URL::route('maingallery') }}">Галерея</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::route('maingallery') }}">Галерея</a></li>
                                    <li><a href="{{ URL::route('mainhonors') }}">Достижения</a></li>
                                    <li><a href="{{ URL::route('mainrepertoire') }}">Репертуар</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="{{ URL::route('byteach') }}">Расписание и набор</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::route('byteach') }}">Расписание</a></li>
                                    <li><a href="{{ URL::route('mainsetagroop') }}">Набор</a></li>
                                </ul>
                            </li>
<!--
                            <li>
                                <a href="{{ URL::route('mainteachers') }}">Преподаватели</a>
                            </li>
-->
                            <li><a href="{{ URL::route('maincontacts') }}">Контакты</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/login">Личный кабинет</a></li>
                        </ul>
<!--
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ URL::route('mainguestbook') }}">Гостевая книга</a></li>
                        </ul>
-->

                        </div>
                    </div>
                </nav>
            </div>
        </header>
        <main>
            <div class="container">
        @yield('sidecontent')
            </div>
        </main>
        {!! Html::script('js/main.js') !!}
    </body>
</html>
