@extends('main.mainlayout') @section('sidecontent')
<main>
    <div class="container col-xs-12">
        <h2>{{ $news->title }}</h2>
        <p><strong>Описание: </strong>{{ $news->body }}</p>
        <img src="{{ $news->picture}}" alt="">
    </div>
</main>
@endsection
