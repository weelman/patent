@extends('main.mainlayout') @section('sidecontent')

<h1 class="page-header">Расписание</h1>
<ul class="nav nav-tabs nav-justified round-tab">
    <li role="presentation"><a href="{{ URL::route('byteach') }}">Список преподавателей</a></li>
    <li role="presentation" class="active"><a href="{{ URL::route('byrooms') }}">Список аудиторий</a></li>
    <li role="presentation"><a href="{{ URL::route('bygroups') }}">Список групп</a></li>
</ul>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"></h2>
    </div>
    <div class="panel-body row">
        <div class="col-sm-4">
            <div id="teachContainer" class="listContainer">
                <ul class="nav nav-pills nav-stacked">
                    @foreach ($room_list as $room_item)
                    <li data-teach="{{ $room_item->id }}" @if($classroom->id == $room_item->id) class="active" @endif>
                        <a href="/byrooms/{{ $room_item->id }}">
                            <center>
                                {{ $room_item->name }}</center>
                        </a>
                    </li>

                    @endforeach
                </ul>
            </div>
            <!-- #teachContainer -->
        </div>
        <div class="col-sm-8">
            <center>
                <p><strong>Адрес: </strong><span id="roomResultEducation">{{ $classroom->address }}</span></p>
            </center>
            <img src="{{ $classroom->picture }}" class="leftimg" style="margin: 0px 10px 10px 0;     width: 700px;"> @if ($room_list->isEmpty() == false)
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Преподаватель</th>
                        <th>Название</th>
                        <th>Описание</th>
                        <th>Частота занятий</th>
                        <th>Тип занятий</th>
                        <th>Время</th>
                        <th>Группа</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($exer_list as $exer_item)
                    <tr style="background-color:{{ $exer_item->color }}">
                        <td>{{ $exer_item->username }}</td>
                        <td>{{ $exer_item->title }}</td>
                        <td>{{ $exer_item->desc }}</td>
                        <td>{{ $exer_item->repeattype }}</td>
                        <td>{{ $exer_item->accesstype }}</td>
                        <td>{{ $exer_item->time }}</td>
                        <td>{{ $exer_item->childgroupname }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>


    </div>

</div>
@endsection