@extends('main.mainlayout') @section('sidecontent')

<h1 class="page-header">Расписание</h1>
<ul class="nav nav-tabs nav-justified round-tab">
    <li role="presentation" class="active"><a href="{{ URL::route('byteach') }}">Список преподавателей</a></li>
    <li role="presentation"><a href="{{ URL::route('byrooms') }}">Список аудиторий</a></li>
    <li role="presentation"><a href="{{ URL::route('bygroups') }}">Список групп</a></li>
</ul>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title"></h2>
    </div>
    <div class="panel-body row">
        <div class="col-sm-4">
            <div id="teachContainer" class="listContainer">
                <ul class="nav nav-pills nav-stacked">
                    @foreach ($user_list as $user_item)
                    <li data-teach="{{ $user_item->id }}" @if($teacher->id == $user_item->id) class="active" @endif>
                        <a href="/byteach/{{ $user_item->id }}">
                            <img src="/img/teach/{{ $user_item->pic }}" class="img-circle teachimg_preview">
                             {{ $user_item->fio }}
                        </a>
                    </li>

                    @endforeach
                </ul>
            </div>
            <!-- #teachContainer -->
        </div>
        <div class="col-sm-8">
            <div id="teachResult">
                <img id="teachResultImg" src="/img/teach/{{ $teacher->pic }}" class="teachimg img-rounded">
                <center>
                    <h3 id="teachResultFio">{{ $teacher->fio }}</h3></center>
                <p><strong>Образование: </strong><span id="teachResultEducation">{{ $teacher->education }}</span></p>

                @if ($exer_list->isEmpty() == false)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Аудитория</th>
                            <th>Частота занятий</th>
                            <th>Тип занятий</th>
                            <th>Время</th>
                            <th>Группа</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($exer_list as $exer_item)
                        <tr style="background-color:{{ $exer_item->color }}">
                            <td>{{ $exer_item->title }}</td>
                            <td>{{ $exer_item->desc }}</td>
                            <td>{{ $exer_item->classroomname }}</td>
                            <td>{{ $exer_item->repeattype }}</td>
                            <td>{{ $exer_item->accesstype }}</td>
                            <td>{{ $exer_item->time }}</td>
                            <td>{{ $exer_item->childgroupname }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection