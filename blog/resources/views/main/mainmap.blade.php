@extends('main.mainlayout')
@section('sidecontent')

            <h1 class="page-header ">Карта сайта</h1>
        </div>
        <div class="container">
            <div class="col-sm-12">
                
                <ul>
                    <li class="list-unstyled">
                        <a href="/">Главная</a>
                    </li>
                    <li class="list-unstyled">
                        <a href="{{ URL::route('mainnews') }}">Новости</a>
                    </li>
                    <li class="list-unstyled">
                        Галерея
                        <ul>
                            <li><a href="{{ URL::route('maingallery') }}">Галерея</a></li>
                            <li><a href="{{ URL::route('mainhonors') }}">Достижения</a></li>
                            <li><a href="{{ URL::route('mainrepertoire') }}">Репертуар</a></li>
                        </ul>
                    </li>
                    <li class="list-unstyled">
                        Расписание и набор
                        <ul>
                            <li><a href="{{ URL::route('maintimetable') }}">Расписание</a></li>
                            <li><a href="{{ URL::route('mainsetagroop') }}">Набор</a></li>
                        </ul>
                    </li>
                    <li class="list-unstyled"><a href="{{ URL::route('mainteachers') }}">Преподаватели</a></li>
                    <li class="list-unstyled"><a href="{{ URL::route('maincontacts') }}">Контакты</a></li>
                </ul>
                
            </div>


@endsection