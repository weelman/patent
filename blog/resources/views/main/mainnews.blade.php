@extends('main.mainlayout') @section('sidecontent')
<h1 class="page-header">Новости</h1>
   @foreach ($news_list as $news_item)
       <h4><a href="/news/{{ $news_item->id }}">{{  $news_item->title }}</a></h4>
    <blockquote style="border-left: 5px solid #e77817;">
        <p>{{ str_limit($news_item->body, 200) }}</p>
        <p class="text-right"><span></span>{{  $news_item->created_at }}</p>
    </blockquote>
@endforeach
@endsection
