<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Exercises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('repeattype')->nullable();
            $table->string('accesstype');
            $table->string('time');
            $table->string('title');
            $table->text('desc');
            $table->text('color')->nullable();

			$table->timestamps();
			$table->softDeletes();

			
            $table->integer('classroom_id')->unsigned()->nullable();
			$table->foreign('classroom_id')->references('id')->on('classrooms');

			$table->integer('childgroup_id')->unsigned()->nullable();
			$table->foreign('childgroup_id')->references('id')->on('childgroups');

			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users');
		 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exercises');
    }
}
