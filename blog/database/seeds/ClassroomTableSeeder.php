<?php

use Illuminate\Database\Seeder;

use App\Classroom;

class ClassroomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		Classroom::create([
			'name'=>'Зал для бальных танцев',
            'address'=>'г. Иркутск, ул. Лермонтова д.256',
            'picture'=>'/img/plan/plan-1.jpg'
		]);
        Classroom::create([
			'name'=>'Кабинет для занятий',
            'address'=>'г. Иркутск, ул. 4-ая Железнодорожная д. 100',
            'picture'=>'/img/plan/plan-2.jpg'
		]);
        Classroom::create([
			'name'=>'Гимнастический зал',
            'address'=>'г. Иркутск, ул. Ломоносова д.1',
            'picture'=>'/img/plan/plan-3.jpg'
		]);
        Classroom::create([
			'name'=>'Концертная сцена',
            'address'=>'г. Иркутск, ул. 2-ая Железнодорожная д.2',
            'picture'=>'/img/plan/plan-4.jpg'
		]);
	}
}
