<?php

use Illuminate\Database\Seeder;

use App\Childgroup;

class ChildgroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Childgroup::create([
			'name'=>'малыши',
            'age'=>'4-7',
		]);
        Childgroup::create([
			'name'=>'Среднячи',
            'age'=>'8-13',
		]);
        Childgroup::create([
			'name'=>'Старшие',
            'age'=>'14-16',
		]);
        Childgroup::create([
			'name'=>'Малыши 2',
            'age'=>'4-7',
		]);
        Childgroup::create([
			'name'=>'Среднячи от 9',
            'age'=>'9-13',
		]);
    }
}
