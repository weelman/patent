<?php

use Illuminate\Database\Seeder;

use App\Exercise;
use App\Classroom;
use App\Childgroup;
use App\User;

class ExerciseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		$teacher1 = User::where('name','charushina')->first();
        $teacher2 = User::where('name','gapshite')->first();
        $teacher3 = User::where('name','anchugova')->first();
        $teacher4 = User::where('name','melnichuk')->first();
        $teacher5 = User::where('name','ershova')->first(); 
		$class = Classroom::first();
        $class1 = Classroom::where('name','Кабинет для занятий')->first();
        $class2 = Classroom::where('name','Гимнастический зал')->first();
		$group = Childgroup::first();
        $group1 = Childgroup::where('name', 'Малыши 2')->first();
        $group2 = Childgroup::where('name', 'Старшие')->first();
		
        Exercise::create([
			'repeattype'=>'ежедневное (пн-пт)',
			'accesstype'=>'для всех',
			'time'=>'17:50',
            'title'=>'Занятия для девочек',
            'desc'=>'Описание занятия',
            'color'=>'rgba(33,255,81, 0.5)',
            'classroom_id'=> $class1->id,
            'childgroup_id'=> $group->id,
            'user_id'=> $teacher3->id,
		]);
		
        Exercise::create([
			'repeattype'=>'Разовое',
			'accesstype'=>'индивидуальное',
			'time'=>'12:50',
            'title'=>'Занятие для одного',
            'desc'=>'Занятие по индивидуальной программе',
            'color'=>'rgba(33,255,81, 0.5)',
            'classroom_id'=> $class2->id,
            'childgroup_id'=> $group1->id,
            'user_id'=> $teacher1->id,
		]);
		
        Exercise::create([
			'repeattype'=>'Еженедельное (вторник)',
			'accesstype'=>'для всех',
			'time'=>'15:30',
            'title'=>'Занятия для мальчиков',
            'desc'=>'Занятия для парней',
            'color'=>'rgba(159,33,255, 0.5)',
            'classroom_id'=> $class->id,
            'childgroup_id'=> $group->id,
            'user_id'=> $teacher1->id,
		]);
        
        Exercise::create([
			'repeattype'=>'Еженедельное (среда)',
			'accesstype'=>'для всех',
			'time'=>'15:10',
            'title'=>'Занятия для всех',
            'desc'=>'Описание',
            'color'=>'rgba(159,33,255, 0.5)',
            'classroom_id'=> $class1->id,
            'childgroup_id'=> $group->id,
            'user_id'=> $teacher2->id,
		]);
        
        Exercise::create([
			'repeattype'=>'Еженедельное (четверг)',
			'accesstype'=>'для всех',
			'time'=>'12:10',
            'title'=>'Занятия для всех',
            'desc'=>'Описание',
            'color'=>'rgba(255,85,33, 0.5)',
            'classroom_id'=> $class->id,
            'childgroup_id'=> $group2->id,
            'user_id'=> $teacher2->id,
		]);
        
        Exercise::create([
			'repeattype'=>'Еженедельное (четверг)',
			'accesstype'=>'для всех',
			'time'=>'12:10',
            'title'=>'Занятия для всех',
            'desc'=>'Описание',
            'color'=>'rgba(255,85,33, 0.5)',
            'classroom_id'=> $class->id,
            'childgroup_id'=> $group->id,
            'user_id'=> $teacher3->id,
		]);
        
        Exercise::create([
			'repeattype'=>'Еженедельное (суббота)',
			'accesstype'=>'Для любого',
			'time'=>'13:20',
            'title'=>'Занятия для всех',
            'desc'=>'Новое занятие',
			'color'=>'rgba(55,255,33, 0.5)',
			'classroom_id'=> $class2->id,
            'childgroup_id'=> $group1->id,
            'user_id'=> $teacher4->id,
		]);
        
        Exercise::create([
			'repeattype'=>'Ежедневное',
			'accesstype'=>'Для опытных',
			'time'=>'17:30',
            'title'=>'Занятия',
            'desc'=>'Новая группа',
			'color'=>'rgba(55,255,33, 0.5)',
            'classroom_id'=> $class1->id,
            'childgroup_id'=> $group2->id,
            'user_id'=> $teacher5->id,
		]);
    }
    
}
