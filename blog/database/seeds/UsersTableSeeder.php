<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		User::create([
			'name'=>'admin',
            'post'=>'Администратор сайта',
			'email'=>'admin@site.ru',
			'password'=>bcrypt('admin'),
		]);
        
        User::create([
			'name'=>'charushina',
            'fio'=>'Чарушина Елена Николаевна',
            'education'=>'-1994 – 1997гг. Иркутское областное училище культуры, отделение Хореографии; -2002-2005 гг. Восточно-Сибирская государственная академия культуры и искусств, отделение Бизнеса и администрирования -«Учитель года 2008» -«Лучший педагог дополнительного образования 2010»',
            'post'=>'Руководитель коллектива',
            'picture'=>'1.jpg',
			'email'=>'charushina@site.ru',
			'password'=>bcrypt('charushina'),
            'is_teacher'=> true,
		]);

        User::create([
            'name'=>'gapshite',
            'fio'=>'Гапшите Елена Павловна',
            'education'=>'-2002 - 2006 Иркутский государственный педагогический колледж №1, специальность ритмика и хореография -2006 – 2011 Иркутский государственный технический университет-Победитель конкурса «Педагогический дебют 2011»-Победитель муниципального конкурса общественного признания «Родительская благодарность 2015»',
            'post'=>'Руководитель подготовительного этапа обучения',
            'picture'=>'2.jpg',
            'email'=>'gapshite@site.ru',
            'password'=>bcrypt('gapshite'),
            'is_teacher'=>true,
        ]);

        User::create([
            'name'=>'anchugova',
            'fio'=>'Анчугова Марина Васильевна',
            'education'=>'-1981 Государственное Бурятское хореографическое училище, классическое отделение -Артистка балета Иркутского музыкального теартра им. Загурского',
            'post'=>'Педагог по классическому танцу',
            'picture'=>'3.jpg',
            'email'=>'anchugova@site.ru',
            'password'=>bcrypt('anchugova'),
            'is_teacher'=>true,
        ]);

		User::create([
            'name'=>'melnichuk',
            'fio'=>'Мельничук Анастасия Сергеевна',
            'education'=>'-2006 – 2010 Иркутский областной колледж  культуры,  отделение хореографии; -2010 Восточно-Сибирская государственная академия культуры и искусств, студентка отделения хореографии.',
            'post'=>'Репетитор ансамбля, педагог подготовительных и хобби групп',
            'picture'=>'4.jpg',
            'email'=>'melnichuk@site.ru',
            'password'=>bcrypt('melnichuk'),
            'is_teacher'=>true,
        ]);
        
        User::create([
            'name'=>'ershova',
            'fio'=>'Ершова Галина Борисовна',
            'education'=>'-Музыкальное педагогическое училище №3 отделение: учитель музыки и музыкальный руководитель',
            'post'=>'Педагог Подготовительных Групп',
            'picture'=>'5.jpg',
            'email'=>'ershova@site.ru',
            'password'=>bcrypt('serdyuk'),
            'is_teacher'=>true,
        ]);
        
        User::create([
            'name'=>'serdyuk',
            'fio'=>'Сердюк Юлия Евгеньевна',
            'education'=>'-2004 - 2008 Иркутский государственный педагогический колледж №1, специальность ритмика и хореография;',
            'post'=>'Концертмейстер',
            'picture'=>'6.jpg',
            'email'=>'serdyuk@site.ru',
            'password'=>bcrypt('serdyuk'),
            'is_teacher'=>true,
        ]);
        
        User::create([
            'name'=>'lavrenteva',
            'fio'=>'Лаврентьева Татьяна Валериевна',
            'education'=>'-2004 г. "Иркутский государственный педагогический университет", специальность: "Учитель начальных классов с доп. специальностью "педагог-психолог"',
            'post'=>'Администратор',
            'picture'=>'7.jpg',
            'email'=>'lavrenteva@site.ru',
            'password'=>bcrypt('lavrenteva'),
            'is_teacher'=>false,
        ]);
        User::create([
            'name'=>'ilyuhina',
            'fio'=>'Ильюхина Анна Вячеславовна',
            'education'=>'',
            'post'=>'Костюмер',
            'picture'=>'8.jpg',
            'email'=>'ilyuhina@site.ru',
            'password'=>bcrypt('ilyuhina'),
            'is_teacher'=>false,
        ]);
}
}
