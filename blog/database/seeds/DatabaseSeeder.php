<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('exercises')->delete();
        DB::table('childgroups')->delete();
		DB::table('classrooms')->delete();
		DB::table('users')->delete();
        DB::table('news')->delete();

		$this->call(NewsitemSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(ClassroomTableSeeder::class);
		$this->call(ChildgroupTableSeeder::class);
		$this->call(ExerciseTableSeeder::class);
				
    }
}
